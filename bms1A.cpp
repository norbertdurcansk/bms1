//
// Created by Norbert on 10/10/2016.
// xdurca01 FIT VUTBR 1MIT 2016/2017
//
#include <iostream>
#include "Encoder.h"

using namespace std;


int main(int argc, char **argv) {
    if (argc != 2) {
        cerr << "Wrong parameters! (./bms1A [file_name])\n";
        exit(1);
    }
    initialize_ecc();
    Encoder *encoder = new Encoder(argv);
    if(!encoder->encodeWords()){
        cerr<<"Cannot load file content\n";
        delete(encoder);
        exit(2);
    };
    encoder->interlacing();
    delete (encoder);
    return 0;
}