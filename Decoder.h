//
// Created by norbert on 2.11.2016.
// xdurca01 FIT VUTBR 1MIT 2016/2017
//

#ifndef BMS1B_DECODER_H
#define BMS1B_DECODER_H

#include <fstream>
#include <bitset>
#include <vector>
#include "ecc.h"
#define BLOCK_SIZE 226
using namespace std;

class Decoder {
public:
    ifstream input;
    ofstream output;
    char *buffer;
    unsigned  int sizeofLastBlock;
    unsigned  int fileSize;
    vector<unsigned  char *> codeArray;
    Decoder( char **);
    ~Decoder();

    bool interlacing();

    void loadBuffer();

    void decodeWords();

    void writeToFile();
};


#endif //BMS1B_DECODER_H
