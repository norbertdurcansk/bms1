//
// Created by norbert on 2.11.2016.
// xdurca01 FIT VUTBR 1MIT 2016/2017
//

#include <iostream>
#include "Decoder.h"

using namespace std;


int main(int argc, char **argv) {
    initialize_ecc();
    if (argc != 2) {
        cerr << "Wrong parameters! (./bms1B [file_name])\n";
        exit(1);
    }
    initialize_ecc();
    Decoder *decoder = new Decoder(argv);
    if(!decoder->interlacing()){
        cerr<<"Cannot load file content\n";
        delete(decoder);
        exit(2);
    }
    decoder->decodeWords();
    decoder->writeToFile();

    delete(decoder);
    return 0;
}