//
// Created by norbert on 2.11.2016.
// xdurca01 FIT VUTBR 1MIT 2016/2017
//

#include <cstring>
#include "Decoder.h"


Decoder::Decoder(char **argv) {
    if (ifstream(argv[1])) {
        input.open(argv[1], ios::binary | ios::in);
        fileSize=(unsigned int)input.seekg(0,input.end).tellg();
        input.seekg(0,input.beg);
        buffer=new char[fileSize];
    }
    string out=argv[1];
    out+=".ok";
    if(ofstream (out)){
        output.open(out.c_str(), ios::binary | ios::out);
    }
}
Decoder::~Decoder() {
    input.close();
    output.close();
    delete(buffer);
}

bool Decoder::interlacing(){
    if(fileSize==0 || !input || !output)
        return false;
    //load file to buffer
    loadBuffer();
    unsigned int blockcount=fileSize/(BLOCK_SIZE);
    sizeofLastBlock=fileSize-(blockcount*(BLOCK_SIZE));
    if(sizeofLastBlock==0)sizeofLastBlock=BLOCK_SIZE;

    for(unsigned int i=0;i<blockcount;i++){
        //create
        unsigned char * codeword=new unsigned char[BLOCK_SIZE];
        codeArray.push_back(codeword);
    }
    if(sizeofLastBlock!=BLOCK_SIZE){
        unsigned char * codeword=new unsigned char[sizeofLastBlock];
        codeArray.push_back(codeword);
    }
        unsigned int i=0;
        unsigned  int z=0;
        for(unsigned int j =0; j<fileSize;j++){
            if(i==codeArray.size()-1){
                if(z<sizeofLastBlock){
                    codeArray.at(i)[z]=buffer[j];
                    (z+1==BLOCK_SIZE)?z=0:z++;
                    i=0;
                }else{
                    (z+1==BLOCK_SIZE)?z=0:z++;
                    i=0;
                    codeArray.at(i)[z] = buffer[j];
                    i++;
                }
            }else{
                codeArray.at(i)[z]=buffer[j];
                i++;
            }
    }
    return true;
}

void Decoder::loadBuffer(){
    input.read(buffer,fileSize);
}

void Decoder::writeToFile(){
    for(unsigned int i=0;i<codeArray.size();i++){
        if(i+1==codeArray.size()) {
            output.write((char *)codeArray.at(i),sizeofLastBlock-NPAR);
        }else{
            output.write((char *)codeArray.at(i),BLOCK_SIZE-NPAR);
        }
    }
}

void Decoder::decodeWords(){

    for(unsigned int i=0;i<codeArray.size();i++) {

        unsigned int blocksize=BLOCK_SIZE;
        if(i+1==codeArray.size())blocksize=sizeofLastBlock;

        decode_data(codeArray.at(i), blocksize);
        if (check_syndrome() != 0) {
            correct_errors_erasures(codeArray.at(i),blocksize,0,NULL);
        }

    }
 }