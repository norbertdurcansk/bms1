//
// Created by norbert on 1.11.BLOCK_SIZE16.
//

#include <cstring>
#include <sstream>
#include "Encoder.h"

Encoder::Encoder(char **argv) {
    if (ifstream(argv[1])) {
        input.open(argv[1], ios::binary | ios::in);
        fileSize=(unsigned int)input.seekg(0,input.end).tellg();
        input.seekg(0,input.beg);
        buffer=new char[fileSize];
    }
    string out=argv[1];
    out+=".out";
    if(ofstream (out)){
        output.open(out.c_str(), ios::binary | ios::out);
    }
}
Encoder::~Encoder() {
    input.close();
    output.close();
    delete(buffer);
}
void Encoder::interlacing() {

    for(unsigned int j=0; j<BLOCK_SIZE+NPAR;j++){
        for(unsigned int i=0;i<codeArray.size();i++) {
            unsigned char *tmpCodeword = codeArray.at(i);
            //last block
            if (i+1==codeArray.size()) {
                if (j >= sizeofLastBlock){
                    if(i==0)break;
                    i=0;
                    tmpCodeword = codeArray.at(i);
                    j++;
                    if(j<BLOCK_SIZE+NPAR){
                        output << tmpCodeword[j];
                    }else{
                        break;
                    }
                }else{
                    output << tmpCodeword[j];
                }
            }else{
                output << tmpCodeword[j];
            }
        }
    }
}
bool Encoder::encodeWords()  {
    if(fileSize==0 || !input || !output)
        return false;
    //load file to buffer
    loadBuffer();
    unsigned int blockcount=fileSize/BLOCK_SIZE;
    sizeofLastBlock=(fileSize-(blockcount*BLOCK_SIZE));
    (sizeofLastBlock==0)?sizeofLastBlock=BLOCK_SIZE+NPAR:sizeofLastBlock+=NPAR;

    for(unsigned int  i=0;i<blockcount;i++){
        strncpy(( char *)word,buffer+(i*(BLOCK_SIZE)),BLOCK_SIZE);
        codeword=new unsigned char[BLOCK_SIZE+NPAR];
        encode_data(word,BLOCK_SIZE,codeword);
        codeArray.push_back(codeword);
    }
    if(blockcount*BLOCK_SIZE!=fileSize){
        strcpy(( char *)word,buffer+(blockcount*BLOCK_SIZE));
        codeword=new unsigned char[sizeofLastBlock];
        encode_data(word,(fileSize-blockcount*BLOCK_SIZE),codeword);
        codeArray.push_back(codeword);
    }
    return true;
}
void Encoder::loadBuffer() {
        input.read(buffer,fileSize);
return;
}
