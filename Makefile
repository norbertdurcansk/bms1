#Makefile for  1. project BMS 

.PHONY = all

CC= g++
CFLAGS = -std=c++11 -Wall -g -Wextra

all: bms1A bms1B exc 

bms1A: Encoder.o bms1A.o libecc.a 
	$(CC) $(CFLAGS) -o bms1A Encoder.o bms1A.o libecc.a

Encoder.o: Encoder.cpp Encoder.h
	$(CC) $(CFLAGS) -c Encoder.cpp


bms1A.o: bms1A.cpp Encoder.h ecc.h
	$(CC) $(CFLAGS) -c bms1A.cpp



bms1B: Decoder.o bms1B.o libecc.a
	$(CC) $(CFLAGS) -o bms1B Decoder.o bms1B.o libecc.a

Decoder.o: Decoder.cpp Decoder.h
	$(CC) $(CFLAGS) -c Decoder.cpp

bms1B.o: bms1B.cpp Decoder.h 
	$(CC) $(CFLAGS) -c bms1B.cpp

exc:
	chmod +x bms1B bms1A

clean:
	$(RM) bms1A bms1B *.o *~
