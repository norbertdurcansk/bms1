//
// Created by norbert on 1.11.2016.
// xdurca01 FIT VUTBR 1MIT 2016/2017
//

#ifndef BMS1A_ENCODER_H
#define BMS1A_ENCODER_H
#include <fstream>
#include <bitset>
#include <vector>
#include "ecc.h"
#define BLOCK_SIZE 130

using namespace std;

class Encoder {
 public:
    ifstream input;
    ofstream output;
    unsigned int fileSize;
    char *buffer;
    unsigned char word[BLOCK_SIZE];
    unsigned char* codeword;
    vector< unsigned char*> codeArray;
    unsigned int sizeofLastBlock;
    bool encodeWords();
    ~Encoder();
    void interlacing();
    Encoder(char **);
private:
    void loadBuffer();
};


#endif //BMS1A_ENCODER_H
