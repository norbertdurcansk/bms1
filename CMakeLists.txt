cmake_minimum_required(VERSION 3.5)
project(bms1A)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra ")
set(SOURCE_FILES  Encoder.cpp  bms1A.cpp Encoder.h)
add_executable(bms1A ${SOURCE_FILES})
target_link_libraries (bms1A  ${CMAKE_SOURCE_DIR}/libecc.a)

set(SOURCE_FILES_B  Decoder.cpp Decoder.h bms1B.cpp)
add_executable(bms1B ${SOURCE_FILES_B})
target_link_libraries(bms1B ${CMAKE_SOURCE_DIR}/libecc.a)
